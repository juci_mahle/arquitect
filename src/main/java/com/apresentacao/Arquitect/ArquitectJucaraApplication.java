package com.apresentacao.Arquitect;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArquitectJucaraApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArquitectJucaraApplication.class, args);
	}

}
